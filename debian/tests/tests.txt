$ catatonit true                            #=> --exit 0
$ catatonit echo HELLO
HELLO
$ catatonit echo WORLD >&2
WORLD
$ catatonit false                           #=> --exit 1

